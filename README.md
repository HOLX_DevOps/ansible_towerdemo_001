# Ansible_TowerDemo_001

Ansible_TowerDemo_001

Demonstration Project for Ansible Tower

This is not a demo in Config Management, but rather shows how you can instead utilize Tower as a general-purpose script launcher, invoking scripts you may already have already written, against some remote execution node.   The ansible script code therefore works as a &#34;glue&#34; to launch your scripts, while Tower serves as the web UI for endusers, technically literate or otherwise, can run things on.