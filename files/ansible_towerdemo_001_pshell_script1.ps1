#-----------------------------------------------------------------------------

<#

.SYNOPSIS
    ansible_towerdemo_001

    A barebones powershell script. We'll echo back some input text.


.DESCRIPTION
    A barebones powershell script. We'll echo back some input text, if provided.


.OUTPUTS
    0        = success
    non-zero = error


.NOTES

    AUTHOR:    Hologic IS DevOps Team


.LINK
    
    N/A
	 
#>

#-----------------------------------------------------------------------------

Param
(
[Parameter(Mandatory=$true)][string]$Input_Text_1 = 'abc',
[Parameter(Mandatory=$true)][string]$Input_Text_2 = 'xyz'
)


#-----------------------------------------------------------------------------

Write-Output ("`n")
Write-Output ("Hello!")
Write-Output ("`n")
Write-Output ("`n")
Write-Output ("Echo1: : " + $Input_Text_1 )
Write-Output ("`n")
Write-Output ("Echo2: : " + $Input_Text_2 )
Write-Output ("`n")
Write-Output ("COMBINED: " + $Input_Text_1 + $Input_Text_2)
Write-Output ("`n")
Write-Output ("Goodbye!")
Write-Output ("`n")

exit(0)

#
## END
